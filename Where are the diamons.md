```R
library('data.table')
```


```R
peaks <- read.csv("peaksTable.txt",
                    header = TRUE, 
                    sep = "\t",
                    stringsAsFactors = FALSE)
peaks_dt <- data.table(peaks)
head(peaks_dt)
```


<table class="dataframe">
<caption>A data.table: 6 × 8</caption>
<thead>
	<tr><th scope=col>peakID</th><th scope=col>chr</th><th scope=col>start</th><th scope=col>end</th><th scope=col>genomic_feature</th><th scope=col>symbol</th><th scope=col>accessPattern</th><th scope=col>enhType</th></tr>
	<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>    10</td><td>chr1 </td><td>  1209129</td><td>  1209454</td><td>UTR5E     </td><td>UBE2J2 </td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>prom-AllCTs</td></tr>
	<tr><td>   100</td><td>chr1 </td><td> 11119992</td><td> 11120732</td><td>UTR5E     </td><td>SRM    </td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>prom-AllCTs</td></tr>
	<tr><td>  1000</td><td>chr1 </td><td>161067526</td><td>161068318</td><td>Intergenic</td><td>KLHDC9 </td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>ubiq-AllCTs</td></tr>
	<tr><td> 10000</td><td>chr4 </td><td> 84376397</td><td> 84377354</td><td>Cds       </td><td>MRPS18C</td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>ubiq-AllCTs</td></tr>
	<tr><td>100000</td><td>chr12</td><td> 66584266</td><td> 66585226</td><td>Intron    </td><td>IRAK3  </td><td>CD34-CD14                     </td><td>trans-CD14 </td></tr>
	<tr><td>100002</td><td>chr12</td><td> 67244817</td><td> 67245331</td><td>Intergenic</td><td>GRIP1  </td><td>CD34                          </td><td>other      </td></tr>
</tbody>
</table>




```R
nrow(peaks_dt)
```


120583



```R
peaks_dt[symbol == "CCR2"]
```


<table class="dataframe">
<caption>A data.table: 9 × 8</caption>
<thead>
	<tr><th scope=col>peakID</th><th scope=col>chr</th><th scope=col>start</th><th scope=col>end</th><th scope=col>genomic_feature</th><th scope=col>symbol</th><th scope=col>accessPattern</th><th scope=col>enhType</th></tr>
	<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>106461</td><td>chr3</td><td>46390227</td><td>46390756</td><td>Intergenic</td><td>CCR2</td><td>CD34         </td><td>other   </td></tr>
	<tr><td>115714</td><td>chr3</td><td>46362760</td><td>46363489</td><td>Intergenic</td><td>CCR2</td><td>CD19         </td><td>ct-CD19 </td></tr>
	<tr><td>130422</td><td>chr3</td><td>46354205</td><td>46354935</td><td>Intergenic</td><td>CCR2</td><td>CD14         </td><td>ct-CD14 </td></tr>
	<tr><td>130423</td><td>chr3</td><td>46359487</td><td>46360188</td><td>Intergenic</td><td>CCR2</td><td>CD14         </td><td>ct-CD14 </td></tr>
	<tr><td>130424</td><td>chr3</td><td>46365536</td><td>46366045</td><td>Intergenic</td><td>CCR2</td><td>CD14         </td><td>ct-CD14 </td></tr>
	<tr><td>130425</td><td>chr3</td><td>46395120</td><td>46396013</td><td>UTR5      </td><td>CCR2</td><td>CD14         </td><td>other   </td></tr>
	<tr><td>130426</td><td>chr3</td><td>46402857</td><td>46403220</td><td>UTR3E     </td><td>CCR2</td><td>CD14         </td><td>ct-CD14 </td></tr>
	<tr><td> 25424</td><td>chr3</td><td>46391019</td><td>46391984</td><td>Intergenic</td><td>CCR2</td><td>CD56-CD3-CD19</td><td>lymphoid</td></tr>
	<tr><td> 42042</td><td>chr3</td><td>46401220</td><td>46401918</td><td>UTR3      </td><td>CCR2</td><td>CD14         </td><td>ct-CD14 </td></tr>
</tbody>
</table>




```R
# These are the dark red, CCR2 diamonds:
peaks_dt[(symbol == "CCR2") & (enhType == "ct-CD14")]
```


<table class="dataframe">
<caption>A data.table: 5 × 8</caption>
<thead>
	<tr><th scope=col>peakID</th><th scope=col>chr</th><th scope=col>start</th><th scope=col>end</th><th scope=col>genomic_feature</th><th scope=col>symbol</th><th scope=col>accessPattern</th><th scope=col>enhType</th></tr>
	<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>130422</td><td>chr3</td><td>46354205</td><td>46354935</td><td>Intergenic</td><td>CCR2</td><td>CD14</td><td>ct-CD14</td></tr>
	<tr><td>130423</td><td>chr3</td><td>46359487</td><td>46360188</td><td>Intergenic</td><td>CCR2</td><td>CD14</td><td>ct-CD14</td></tr>
	<tr><td>130424</td><td>chr3</td><td>46365536</td><td>46366045</td><td>Intergenic</td><td>CCR2</td><td>CD14</td><td>ct-CD14</td></tr>
	<tr><td>130426</td><td>chr3</td><td>46402857</td><td>46403220</td><td>UTR3E     </td><td>CCR2</td><td>CD14</td><td>ct-CD14</td></tr>
	<tr><td> 42042</td><td>chr3</td><td>46401220</td><td>46401918</td><td>UTR3      </td><td>CCR2</td><td>CD14</td><td>ct-CD14</td></tr>
</tbody>
</table>




```R
# The light red, CCR2 diamond is somewhere in the following (I bet it's the accessPattern=="CD14" peak, #130425):
peaks_dt[(symbol == "CCR2") & (enhType != "ct-CD14")]
```


<table class="dataframe">
<caption>A data.table: 4 × 8</caption>
<thead>
	<tr><th scope=col>peakID</th><th scope=col>chr</th><th scope=col>start</th><th scope=col>end</th><th scope=col>genomic_feature</th><th scope=col>symbol</th><th scope=col>accessPattern</th><th scope=col>enhType</th></tr>
	<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>106461</td><td>chr3</td><td>46390227</td><td>46390756</td><td>Intergenic</td><td>CCR2</td><td>CD34         </td><td>other   </td></tr>
	<tr><td>115714</td><td>chr3</td><td>46362760</td><td>46363489</td><td>Intergenic</td><td>CCR2</td><td>CD19         </td><td>ct-CD19 </td></tr>
	<tr><td>130425</td><td>chr3</td><td>46395120</td><td>46396013</td><td>UTR5      </td><td>CCR2</td><td>CD14         </td><td>other   </td></tr>
	<tr><td> 25424</td><td>chr3</td><td>46391019</td><td>46391984</td><td>Intergenic</td><td>CCR2</td><td>CD56-CD3-CD19</td><td>lymphoid</td></tr>
</tbody>
</table>




```R
peaks_dt[symbol == "IGLL1"]
```


<table class="dataframe">
<caption>A data.table: 8 × 8</caption>
<thead>
	<tr><th scope=col>peakID</th><th scope=col>chr</th><th scope=col>start</th><th scope=col>end</th><th scope=col>genomic_feature</th><th scope=col>symbol</th><th scope=col>accessPattern</th><th scope=col>enhType</th></tr>
	<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>105841</td><td>chr22</td><td>23922351</td><td>23923858</td><td>UTR5E     </td><td>IGLL1</td><td>CD34                          </td><td>other      </td></tr>
	<tr><td>105842</td><td>chr22</td><td>23925910</td><td>23926463</td><td>Intergenic</td><td>IGLL1</td><td>CD34                          </td><td>other      </td></tr>
	<tr><td> 46673</td><td>chr22</td><td>23883988</td><td>23884186</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc-CD3-CD19               </td><td>other      </td></tr>
	<tr><td> 79485</td><td>chr22</td><td>23875332</td><td>23876437</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc                        </td><td>other      </td></tr>
	<tr><td> 79486</td><td>chr22</td><td>23907516</td><td>23908075</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc                        </td><td>other      </td></tr>
	<tr><td>  8746</td><td>chr22</td><td>23864061</td><td>23864368</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>ubiq-AllCTs</td></tr>
	<tr><td>  8747</td><td>chr22</td><td>23881801</td><td>23882045</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>ubiq-AllCTs</td></tr>
	<tr><td>  8748</td><td>chr22</td><td>23933376</td><td>23933578</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>ubiq-AllCTs</td></tr>
</tbody>
</table>




```R
# These are the dark blue, IGLL1 diamonds:
peaks_dt[(symbol == "IGLL1") & (accessPattern == "CD34")]
```


<table class="dataframe">
<caption>A data.table: 2 × 8</caption>
<thead>
	<tr><th scope=col>peakID</th><th scope=col>chr</th><th scope=col>start</th><th scope=col>end</th><th scope=col>genomic_feature</th><th scope=col>symbol</th><th scope=col>accessPattern</th><th scope=col>enhType</th></tr>
	<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>105841</td><td>chr22</td><td>23922351</td><td>23923858</td><td>UTR5E     </td><td>IGLL1</td><td>CD34</td><td>other</td></tr>
	<tr><td>105842</td><td>chr22</td><td>23925910</td><td>23926463</td><td>Intergenic</td><td>IGLL1</td><td>CD34</td><td>other</td></tr>
</tbody>
</table>




```R
# The light blue, IGLL1 diamonds are somewhere in the following (I bet they are in the accessPattern=="ubiq-AllCTs" class):
peaks_dt[(symbol == "IGLL1") & (accessPattern != "CD34")]
```


<table class="dataframe">
<caption>A data.table: 6 × 8</caption>
<thead>
	<tr><th scope=col>peakID</th><th scope=col>chr</th><th scope=col>start</th><th scope=col>end</th><th scope=col>genomic_feature</th><th scope=col>symbol</th><th scope=col>accessPattern</th><th scope=col>enhType</th></tr>
	<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>46673</td><td>chr22</td><td>23883988</td><td>23884186</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc-CD3-CD19               </td><td>other      </td></tr>
	<tr><td>79485</td><td>chr22</td><td>23875332</td><td>23876437</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc                        </td><td>other      </td></tr>
	<tr><td>79486</td><td>chr22</td><td>23907516</td><td>23908075</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc                        </td><td>other      </td></tr>
	<tr><td> 8746</td><td>chr22</td><td>23864061</td><td>23864368</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>ubiq-AllCTs</td></tr>
	<tr><td> 8747</td><td>chr22</td><td>23881801</td><td>23882045</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>ubiq-AllCTs</td></tr>
	<tr><td> 8748</td><td>chr22</td><td>23933376</td><td>23933578</td><td>Intergenic</td><td>IGLL1</td><td>H1hesc-CD34-CD14-CD56-CD3-CD19</td><td>ubiq-AllCTs</td></tr>
</tbody>
</table>




```R

```
